const arrList = createArray();
const newList = document.createElement('ol');
createItem();
document.body.appendChild(newList);
const div = createDiv();
let timeSec = 10;
runTimer(timeSec);

function createArray(){
	let arr = [];
	let num = prompt("Please, enter a number of items in the list: ", 0);
	for(let i = 1; i <= num; i++){
		arr.push(prompt(`Please, enter a content of item ${i}:`));
	}
	return arr;
}
function createItem(){
	arrList.map(function(item){
		let li = document.createElement('li');
		li.innerHTML = item;
		newList.appendChild(li);
		return;	
	});
}
function createDiv(){
	let div = document.createElement('div');
	div.classList.add('time-off');
	div.id = "timer";
	document.body.appendChild(div);
	return div;
}
function runTimer(timeSec){
	let timerId = setInterval(function() {
		document.getElementById('timer').textContent = timeSec--;
	}, 1000);

	setTimeout(function() {
		clearInterval(timerId);
		newList.remove();
		document.getElementById("timer").remove();
	}, 11000);
}